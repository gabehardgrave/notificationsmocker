# Notifications Mocker

This application is for generating mock fcm notifications for testing downstream.

## Dependencies

Python 2.7, and the [requests library](http://docs.python-requests.org/en/master/). You may also need to download Enumerated types. pip install with requirements.txt should handle everything you.

## Usage

To use the notifications mocker, simply run


`python driver.py <input_file>.txt`


from the command line.

This will parse the input file, and generate notifications based on the given configurations.

The input should be a simple csv file, where each line gives `driver.py` instructions on generating a set of mocked notifications. The first entry in a row tells `driver.py` which notification types to send.

* `run` => This means notifications will be sent based on a mocked run, based on the parameters you pass in. You should see "start run", "end run", and "mile marker" notifications on your phone, in logical order. Examples:
     * `run, user=beacon001, n=10, epoch=23/6/2017, lat=39.1614, lon=-76.7104`. Here, all input parameters are specified. This means that **10** notifications will be sent, based on a mock run for **beacon001**, starting at the specified coordinates, on the specified date.
     * `run, user=beacon001`. This is the minimum possible configuration. The start date will default to the current time, n will default to a number specified in `config.py`, and the initial gps coordinates will default to ProObject's HQ.
     * At a minimum, you must specify the user (`user=some guy`). The order of the pairs is also unimportant. (e.g. `run, user=some guy, lat=somewhere, n=some number` is just as good).
     * Note that notifications for mocked runs might look . . . weird. You aren't guaranteed to get a run that stays on a path (like a street, or dirt road). The run path might also be zig-zaggy. This shouldn't be a problem for downstream testing, since you can still log and verify the gps coordinates are getting mapped on the device correctly. But it is worth noting.


* `resync` => This means resync notifications will be sent for each user you specify. Example:
    * `resync, beacon001, beacon002` will send resync notifications for beacon001 and beacon002.

* `new data` => This means new data notifications will be sent for each user you specify. The structure is *exactly* the same as `resync`. Example:
    * `new data, beacon001, beacon002` will send new data notifications for beacon001 and beacon002.

Here is an example input file.

```
run, user=beacon002
run, user=beacon001, n=10, epoch=23/6/2017, lat=39.1614, lon=-76.7104
resync, beacon001, beacon002
new data, beacon001, beacon002
```

Note that lines that are strictly whitespace are ignored entirely. In addition, the order of entries in the config file are unimportant. Notifications from each instruction set are sent in random order (so in the above example, resync data for beacon001 might be sent before a new_run notification for beacon001). 

**Important Note:** *Each of these notification types were based on legacy code. There is currently no external documentation on the different types of notifications that need to be sent, or what their purposes are. As such, some inferences had to be made. The current notification sent may not be exhaustive, or it might be overkill. If you think anything needs changing, let me know.*


## Configuration

For tuning, refer to `config.py`. Numerous, application related constants are defined here, and can be changed to better suite your needs. Most should be fairly intuitive, but some are worth an explanation or two.

```python
FCM_URL = " . . ."
HEADERS = { . . . }
```

These relate to Firebase Messaging, and are intrinsically linked to the current FCM api. As such, there is really **no reason whatsoever** to modify these values, unless the fcm api changes, or (more likely) our authorization key changes.


```python
DEFAULT_DELAY = # . . .
```

This specifies a constant amount of time, in seconds, between notifications. Right now the current value is set to 1. You can set this to zero if you like, although if the Runnerbuddy App is not set up for it, or you want more realistic notification speeds, you should set this to a positive number.

```python
DEFAULT_PREC = # . . .
```

This specifies the number of decimal places gps coordinates will be in. In theory, this can be any number you want. In practice, too small a value gives you poor data, and too large a value just uses more bandwidth unnecessarily.

```python
*_COLLAPSE_KEY = " . . . "
```

Collapse keys are part of the fcm api, and tell fcm that messages with the same key can "collapse in" on each other, so that fcm only has to send the most recent message. This is an important part of the fcm notification system for reducing congestion. Right now, only two notification types actually use collapse keys, and their values are, frankly, almost completely arbitrary. The only requirement is that no two notification types should share the same collapse key. 