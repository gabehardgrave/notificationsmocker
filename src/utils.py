"""
utils.py

Gabriel Hardgrave

Misc. auxilary functions/datastructures
"""
from time import time, mktime, sleep
from datetime import datetime
from random import SystemRandom
from decimal import Decimal


class RandChain(object): # pylint: disable=too-few-public-methods
    """
    Similar to itertools.chain, but yields elements from each iterator in
    random order. Order of individual elements in each iterator is still
    preserved.
    """

    def __init__(self, iterables, delay=0):
        """
        Params:
            Iterable: An iterable collection of iterables (sooooo meta)
            int: how long to delay between each iteration. Optional
        """
        self.iterators = [iter(i) for i in iterables]
        self.rng = SystemRandom()
        self.delay = int(delay)

    def __iter__(self):
        return self

    def next(self):
        """
        What does the iterator know? Does it know things? Lets find out!
        """
        while len(self.iterators) is not 0:

            i = self.rng.randrange(0, len(self.iterators))
            iterator = self.iterators[i]
            try:
                n = iterator.next()
                sleep(self.delay)
                return n
            except StopIteration:
                del self.iterators[i]
        
        raise StopIteration()
            

def current_time():
    """Returns current unix timestamp (int)"""
    return int(time())


def to_timestamp(t):
    """
    Params:
        str | int: time representation, either int or date
    Returns:
        int: corresponding timestamp
    Raises:
        ValueError: if conversion cannot be done
    """
    timestamp = None
    
    try:
        timestamp = int(t)
    except ValueError:
        pass
    
    if isinstance(timestamp, int):
        if timestamp >= 0:
            return timestamp
        raise ValueError("{} is not a valid timestamp".format(timestamp))

    # add more formats for modularity, but worse performance
    fmts = ["%d/%m/%Y", "%Y/%m/%d"]
    for fmt in fmts:
        try:
            return int(mktime(datetime.strptime(t, fmt).timetuple()))
        except ValueError:
            pass

    raise ValueError("'{}' cannot be matched to a date format".format(t))


def rand_int(beg=0, end=2):
    """RNG, for ints"""
    return SystemRandom().randrange(beg, end)

def rand_dec(beg=0, end=1):
    """RNG, for Decimal"""
    return Decimal(SystemRandom().uniform(beg, end))


# Ad hoc testing
if __name__ == "__main__":
    pass
