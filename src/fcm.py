"""
fcm.py

Gabriel Hardgrave

FCM packet and messaging related infrastructure
"""

from time import sleep
from requests import post
from requests.exceptions import RequestException

from config import FCM_URL, HEADERS


def jsonify(nfdata):
    """
    Arg:
        NfData: notification data
    Returns:
        dict: json representation, for fcm notifications/messages
    """
    assert(nfdata.nftype.include_gps() or 
           (nfdata.lat is None and nfdata.lon is None))
    assert((nfdata.lat is None and nfdata.lon is None) or
           nfdata.nftype.include_gps())

    json = {
        "to": "/topics/" + nfdata.user,
        "time_to_live": nfdata.nftype.ttl(),
        "notification" : {
            "title" : nfdata.nftype.title(),
            "body" : nfdata.user + " " + nfdata.nftype.body()
        },
        "data" : {
            "timestamp" : nfdata.tstamp,
            "data_type" : nfdata.nftype.name
        }
    }

    if nfdata.nftype.include_gps():
        assert(nfdata.lat is not None and nfdata.lon is not None)
        json["data"]["latitude"] = float(nfdata.lat)
        json["data"]["longitude"] = float(nfdata.lon)

    if nfdata.nftype.is_collapsible():
        json["collapse_key"] = nfdata.nftype.collapse_key()

    return json



def send_notification(notification):
    """
    Args:
        nfdata: notification to send
    Returns:
        bool, str: tuple indicating a) whether the request was sent
                   successfully, and b) a descriptive error/success message
    """
    data = jsonify(notification)
    backoff = 1
    while 1: # marginally faster than while true, in python 2 at least
        
        try:
            response = post(FCM_URL, headers=HEADERS, json=data, timeout=5)
            status = response.status_code
        except RequestException as e:
            return (False, str(e))

        if status == 200:
            # return str is somewhat arbitrary
            return (True, "{} sent successfully".format(notification))
        
        elif 499 < status < 600: # Uh oh, congestion!
            backoff *= 2
            sleep(backoff)

        elif status == 401:
            return (False, "URGENT: Authentication Error")

        elif status == 400:
            return (False, "Fix JSON format")

        else:
            err = "HTTP response code '{}' not in FCM spec".format(status)
            raise Exception(err)



# ad hoc testing
if __name__ == "__main__":
    pass
    #for data in NfRunGen("beacon001", 4):
    #    print send_notification(data)
