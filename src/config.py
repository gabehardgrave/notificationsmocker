"""
config.py

Constants and other configure-ables. Use responsibly.
"""
from decimal import Decimal

# -- FCM Type Constants ----------------------------------------------------
# Related to FCM messaging. There should really be no reason to change these
# unless FCM undergoes an API overhall, or our server authentication key
# changes.

FCM_URL = "https://fcm.googleapis.com/fcm/send"

#pylint: disable=line-too-long
HEADERS = {
    'Authorization' : 'key=AAAAVZWUUSA:APA91bE_QL8_ivudswMCmbRAjDsUw3lqU8LSOu2gj9G7lLMycL3dvWjx-wFgREFIZt1j6_KJpZ_nXuA3U4s7Fx-OoiKBOFZ9ULh38cmDklnPdM-CIF-eXo27BrmEMb3-kih6O-mME7Hg',
    'Content-Type' : 'application/json'
}


# -- Mock Data constants ---------------------------------------------------
# Semi-arbitrary, used in constructing Mock Data. Alter as you see fit

DEFAULT_LAT = Decimal("39.161400") # ProObject HQ
DEFAULT_LONG = Decimal("-76.710400")

MIN_RUN_POINTS = 5
MAX_RUN_POINTS = 20

TIME_DIFF = 1500
LONG_DIFF = 0.003
LAT_DIFF = 0.003

DEFAULT_DELAY = 0.5 # seconds!

DEFAULT_PREC = 7 # number of places we care about for longitude and latitude

# -- Notification Type constants -------------------------------------------
# Modify these to change the output and/or behavior of certain notification
# types

# Titles
NEW_RUN_TITLE = 'New Run'
STARTED_RUN_TITLE = 'Started Run'
ENDED_RUN_TITLE = 'Ended Run'
MILE_MARKER_TITLE = 'Reached a Mile marker'
NEW_DATA_TITLE = 'New Data'
RESYNC_DATABASE_TITLE = 'Resync with database'

# Body Texts
NEW_RUN_MSG = 'started a new run.'
STARTED_RUN_MSG = 'started a run.'
ENDED_RUN_MSG = 'ended a run.'
MILE_MARKER_MSG = 'Reached a Mile marker.'
NEW_DATA_MSG = 'new data.'
RESYNC_DATABASE_MSG = 'resynced with database.'

# Time - To - Live, in seconds!
NEW_RUN_TTL = 43200 # twelve hours
STARTED_RUN_TTL = 900 # fifteen minutes
ENDED_RUN_TTL = 10800 # three hours
MILE_MARKER_TTL = 0 # one hundred million years!
NEW_DATA_TTL = 900 # fifteen minutes
RESYNC_DATABASE_TTL = 0 # no, two hundred million years!

# Collapse keys (i.e. whether duplicate notifications can be discarded)
NEW_DATA_COLLAPSE_KEY = "a"
RESYNC_DATABASE_COLLAPSE_KEY = "b"


