"""
notification.py

Gabriel Hardgrave

Generating mocked data, and representation of notification data
"""
from decimal import Decimal
from enum import Enum, unique
from collections import namedtuple

from utils import current_time, to_timestamp, rand_int, rand_dec
from config import (NEW_RUN_TITLE, STARTED_RUN_TITLE, ENDED_RUN_TITLE,
                    MILE_MARKER_TITLE, NEW_DATA_TITLE, RESYNC_DATABASE_MSG,
                    RESYNC_DATABASE_TITLE, NEW_RUN_MSG, STARTED_RUN_MSG,
                    ENDED_RUN_MSG, MILE_MARKER_MSG, NEW_DATA_MSG,
                    NEW_RUN_TTL, STARTED_RUN_TTL, ENDED_RUN_TTL,
                    MILE_MARKER_TTL, NEW_DATA_TTL, RESYNC_DATABASE_TTL,
                    NEW_DATA_COLLAPSE_KEY, RESYNC_DATABASE_COLLAPSE_KEY,
                    DEFAULT_LONG, DEFAULT_LAT, MIN_RUN_POINTS,
                    MAX_RUN_POINTS, LONG_DIFF, LAT_DIFF, TIME_DIFF)

# -- DataStructures --------------------------------------------------------

# exploit internal int representation of NfType for quick lookups
# tables should be consistent w/NfType values, or else!
_TTL_TABLE = (NEW_RUN_TTL,
              STARTED_RUN_TTL,
              ENDED_RUN_TTL,
              MILE_MARKER_TTL,
              NEW_DATA_TTL,
              RESYNC_DATABASE_TTL)
assert(len(_TTL_TABLE) == 6)

_TITLE_TABLE = (NEW_RUN_TITLE,
                STARTED_RUN_TITLE,
                ENDED_RUN_TITLE,
                MILE_MARKER_TITLE,
                NEW_DATA_TITLE,
                RESYNC_DATABASE_TITLE)
assert(len(_TITLE_TABLE) == 6)

_BODY_TABLE = (NEW_RUN_MSG,
               STARTED_RUN_MSG,
               ENDED_RUN_MSG,
               MILE_MARKER_MSG,
               NEW_DATA_MSG,
               RESYNC_DATABASE_MSG)
assert(len(_BODY_TABLE) == 6)

_COLLAPSE_KEY_TABLE = (None, None, None, None,
                       NEW_DATA_COLLAPSE_KEY, RESYNC_DATABASE_COLLAPSE_KEY)
assert(len(_COLLAPSE_KEY_TABLE) == 6)


@unique
class NfType(Enum):
    """notification types and associated payload constants"""
    # NOTE: At the time of writing this, all types are taken from legacy
    # code, although there is currently no documentation on what types of
    # notification need to be supported, or what the differences are
    # between them
    new_run = 1
    start_run = 2
    end_run = 3
    mile_marker = 4
    new_data = 5
    resync = 6

    def ttl(self):
        """
        Returns:
            int: time to live for the notification, in seconds
        """
        assert(1 <= self.value <= 6)
        return _TTL_TABLE[self.value - 1]


    def title(self):
        """
        Returns:
            str: title for the notification
        """
        assert(1 <= self.value <= 6)
        return _TITLE_TABLE[self.value - 1]

    def body(self):
        """
        Returns:
            str: message body for the notification
        """
        assert(1 <= self.value <= 6)
        return _BODY_TABLE[self.value - 1]

    def include_gps(self):
        """
        Returns:
            bool: indicating whether to include GPS coordinates
        """
        return self not in (NfType.new_data, NfType.resync)

    def is_collapsible(self):
        """
        Returns:
            bool: indicating whether to use collape_key
        """
        return self in (NfType.new_data, NfType.resync)

    def collapse_key(self):
        """
        Returns:
            str: unique collaps key for the nftype
        Preconditions:
            Make sure self.is_collapsible(), otherwise very bad no good
            terrible things could happen
        """
        assert(self.is_collapsible())
        assert(1 <= self.value <= 6)
        return _COLLAPSE_KEY_TABLE[self.value - 1]


# Data for individual Notifications
NfData = namedtuple("NfData", ["user", "nftype", "tstamp", "lat", "lon"])

class NfResyncGen(object): # pylint: disable=too-few-public-methods
    """Generator for resync notifications for specified users"""
    
    def __init__(self, users=None):
        users = [] if users is None else users
        self.user_iter = iter(users)

    def __iter__(self):
        return self

    def next(self):
        """
        Returns:
            NfData: Resync notification for the next user
        Raises:
            StopIteration: 
        """
        next_user = self.user_iter.next()
        return NfData(user=next_user,
                      nftype=NfType.resync,
                      tstamp=current_time(),
                      lat=None,
                      lon=None)


class NfNewDataGen(object): # pylint: disable=too-few-public-methods
    """Generator for new data notifications for specified users"""
    
    def __init__(self, users=None):
        users = [] if users is None else users
        self.user_iter = iter(users)

    def __iter__(self):
        return self

    def next(self):
        """
        Returns:
            NfData: new data notification for the next user
        Raises:
            StopIteration: 
        """
        next_user = self.user_iter.next()
        return NfData(user=next_user,
                      nftype=NfType.new_data,
                      tstamp=current_time(),
                      lat=None,
                      lon=None)


class NfRunGen(object): # pylint: disable=too-few-public-methods
    """Generator for NfData over a random run"""
    
    # pylint: disable=too-many-arguments
    def __init__(self, user, n=None, epoch=None, lat=None, lon=None):
        """
        Params:
            str: User the mock notifications apply to
            Option[int]: Number of notifications to generate. Defaults to a
                         random, positive number.
            Optional[str | int]: A date for the notifications, defaults to
                                 current time if unspecified
            Optional[Decimal]: initial latitude, otherwise defaults to
                               config
            Optional[Decimal]: initial longitude, otherwise defaults to
                               config
        Raises:
            ValueError: if epoch cannot be converted into a valid date,
                        n < 0, or other params are nonconformant
        """
        if n is not None and n < 0:
            raise ValueError("Cannot have {} iterations".format(n))

        self.username = str(user)

        self.lon = DEFAULT_LONG if lon is None else Decimal(lon)
        self.lat = DEFAULT_LAT if lat is None else Decimal(lat)

        self.timestamp = (current_time() if epoch is None
                          else to_timestamp(epoch))
        assert(self.timestamp >= 0)

        self.stop = (rand_int(MIN_RUN_POINTS, MAX_RUN_POINTS) if n is None
                     else int(n))
        assert(self.stop > 0)
        
        self.nftype = NfType.new_run

    
    def __iter__(self):
        assert(self.stop >= 0 and self.timestamp >= 0)
        return self


    def next(self):
        """
        Returns:
            NfData: Next logical notification in the run
        Raises:
            StopIteration: 
        """
        assert(self.stop >= 0 and self.timestamp >= 0)

        if self.stop is 0:
            raise StopIteration()
        
        self.stop -= 1

        value = NfData(user=self.username,
                       nftype=self.nftype,
                       tstamp=self.timestamp,
                       lat=self.lat,
                       lon=self.lon)

        self._incr_marker()

        self.nftype = (NfType.end_run if self.stop is 1
                       else NfType.mile_marker)
        return value


    def _incr_marker(self):
        self.timestamp += rand_int(1, TIME_DIFF)
        self.lon += rand_dec(-LONG_DIFF, LONG_DIFF)
        self.lat += rand_dec(-LAT_DIFF, LAT_DIFF)


# -- Ad hoc testing --------------------------------------------------------
if __name__ == "__main__":
    pass
