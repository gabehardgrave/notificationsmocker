"""
driver.py

Gabriel Hardgrave


Driver for sending mock notifications, w/ the specified input file.
"""

from decimal import getcontext
import sys

from utils import RandChain
from fcm import send_notification
from config import DEFAULT_DELAY, DEFAULT_PREC
from notification import NfRunGen, NfNewDataGen, NfResyncGen


def parse_input_file(filename):
    """
    Param:
        str: filename to parse
    Returns:
        list: generators specified by the config file
    Raises:
        IOError: file cannot be accessed
        ValueError: file is improperly formatted
    """
    f = open(filename, "r")
    lines = f.read().split("\n")
    f.close()

    generators = []

    for line in lines:
        if line.strip() == "": # ignore pure whitespace
            continue

        elems = [elem.strip() for elem in line.split(",")]
        
        if len(elems) < 2:
            raise ValueError("'{}' needs more entries".format(line))
        
        start = elems[0].lower()
        elems = elems[1:]
        gen = None

        if start in ("run", "runs"):
            params = {"user": None, "n": 10, "epoch": None,
                      "lon": None, "lat": None}

            for elem in elems:
                if elem.strip() == "":
                    continue
                
                items = [i.strip() for i in elem.split("=")]
                if len(items) != 2:
                    raise ValueError("Need key-value pair in {}".format(elem))
                params[items[0].lower()] = items[1]
            
            
            gen = NfRunGen(**params)

        elif start in ("resync"):
            gen = NfResyncGen(elems)
        elif start in ("newdata", "new data", "new-data", "new_data"):
            gen = NfNewDataGen(elems)
        else:
            raise ValueError("'{}' is invalid start".format(elems[0]))

        generators.append(gen)


    return generators


def main():
    """
    Constructs notification generators from the input file, and begins
    sending notifications.
    """
    getcontext().prec = DEFAULT_PREC

    if len(sys.argv) != 2:
        print "Enter in 1 input file, por favor"
        return

    try:
        nf_generators = parse_input_file(str(sys.argv[1]))

        for nf in RandChain(nf_generators, delay=DEFAULT_DELAY):
            print send_notification(nf)

    except (IOError, ValueError) as e:
        print e
        return


if __name__ == "__main__":
    main()
